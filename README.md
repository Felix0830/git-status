# gitlab代码统计

## 环境

* python3

## 获取token

1. 登录gitlab, 使用一个权限大的账号；
2. 点击右上角->Settings
3. 左侧选择`AccessToken`, 输入token名称， 选择权限范围，这里我们勾选api:
4. 点击`Create personal access token` 按钮, 复制下来生成的个人账号访问token
## 统计代码量

##### Usage

`git-status.py`脚本为代码量统计脚本，使用语法：

```bash
$ python3 git-status.py
```